var assert = require('assert');
var auiversion = require('../index');
describe ('auiversion', function() {
  it('test', function () {
    assert.equal(auiversion.test('5.7.44-SNAPSHOT', 1).releaseVersion, '0.0.0-5-7-44-SNAPSHOT-001-do-not-use');
    assert.equal(auiversion.test('5.7.44', 1).releaseVersion, '0.0.0-5-7-44-001-do-not-use');
    assert.equal(auiversion.test('5.7.44-SNAPSHOT', 2).nextVersion, '5.7.44-SNAPSHOT');
    assert.equal(auiversion.test('5.7.44-SNAPSHOT', 23).releaseVersion, '0.0.0-5-7-44-SNAPSHOT-023-do-not-use');
    assert.equal(auiversion.test('5.7.0-SNAPSHOT', 1).releaseVersion, '0.0.0-5-7-0-SNAPSHOT-001-do-not-use');
    assert.equal(auiversion.test('5.7.0-SNAPSHOT', 1).releaseVersion, '0.0.0-5-7-0-SNAPSHOT-001-do-not-use');
  });

  it('named', function () {
    assert.equal(auiversion.named('5.7.44-SNAPSHOT', 1, 'DP-702-docs-staging-stable').releaseVersion, '0.0.0-5-7-44-SNAPSHOT-001-DP-702-docs-staging-stable');
    assert.equal(auiversion.named('5.7.44-SNAPSHOT', 2, 'DP-702-docs-staging-stable').releaseVersion, '0.0.0-5-7-44-SNAPSHOT-002-DP-702-docs-staging-stable');
    assert.equal(auiversion.named('5.7.44-SNAPSHOT', 1).nextVersion, '5.7.44-SNAPSHOT');
    assert.equal(auiversion.named('5.7.44-SNAPSHOT', 1, 'epic/DP-322-epic-issue').releaseVersion, '0.0.0-5-7-44-SNAPSHOT-001-epic-DP-322-epic-issue');
  });

  it('alpha', function () {
    assert.equal(auiversion.alpha('7.7.0-SNAPSHOT', 3).nextVersion, '7.7.0-SNAPSHOT');
    assert.equal(auiversion.alpha('7.7.0-SNAPSHOT', 3).releaseVersion, '7.7.0-alpha-3');
    assert.equal(auiversion.alpha('7.7.0-SNAPSHOT').releaseVersion, '7.7.0-alpha-1');
    assert.equal(auiversion.alpha('7.17.44-SNAPSHOT', 155).releaseVersion, '7.17.44-alpha-155');
  });

  it('beta', function () {
    assert.equal(auiversion.beta('7.7.0-SNAPSHOT', 3).nextVersion, '7.7.0-SNAPSHOT');
    assert.equal(auiversion.beta('7.7.0-SNAPSHOT', 3).releaseVersion, '7.7.0-beta-3');
    assert.equal(auiversion.beta('7.7.0-SNAPSHOT').releaseVersion, '7.7.0-beta-1');
    assert.equal(auiversion.beta('7.17.44-SNAPSHOT', 155).releaseVersion, '7.17.44-beta-155');
  });

  it('patch', function () {
    assert.equal(auiversion.patch('5.7.44-SNAPSHOT').releaseVersion, '5.7.44');
    assert.equal(auiversion.patch('5.7.44-SNAPSHOT').nextVersion, '5.7.45-SNAPSHOT');
  });

  it('minor', function () {
    assert.equal(auiversion.minor('5.7.44-SNAPSHOT').releaseVersion, '5.8.0');
    assert.equal(auiversion.minor('5.7.44-SNAPSHOT').nextVersion, '5.9.0-SNAPSHOT');
  });

  it('major', function () {
    assert.equal(auiversion.major('5.7.44-SNAPSHOT').releaseVersion, '6.0.0');
    assert.equal(auiversion.major('5.7.44-SNAPSHOT').nextVersion, '7.0.0-SNAPSHOT');
  });
});
